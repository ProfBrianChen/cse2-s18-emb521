//////////////
// Nell Beatty
// hw05
// 3/6



import java.util.Scanner; 

public class Hw05 {
 
  public static void main(String[] args) {
   
    
    Scanner myScanner = new Scanner ( System.in );
  
    
   
      //course number
    System.out.println("Please enter the course number");
    
     boolean input1 = myScanner.hasNextInt(); 
    
    while (input1 != true){
      System.out.println("Please enter the course number");
      myScanner.nextLine();
      input1 = myScanner.hasNextInt();
      }
    
    int courseNumber = myScanner.nextInt();
      
      System.out.println(courseNumber);
    
    
    
      //department name
    System.out.println("Please enter the department name");
      
    boolean input2 = myScanner.hasNext(); 
    
    while (input2 != true){
      System.out.println("Please enter the department name");
      myScanner.nextLine();
      input2 = myScanner.hasNext();
      }
    
    String departmentName = myScanner.next();
      
      System.out.println(departmentName);
    
    
    
    
      //number of times class meets per week
    System.out.println("Please enter the number of times the class meets per week");
      
     boolean input3 = myScanner.hasNextInt(); 
    
    while (input3 != true){
      System.out.println("Please enter the number of times the class meets per week");
      myScanner.nextLine();
      input3 = myScanner.hasNextInt();
      }
    
    int timesPerWeek = myScanner.nextInt();
      
      System.out.println(timesPerWeek);

          
    
    
      //time class starts 
    
    System.out.println("Please enter the time the class meets");
    
    boolean input4 = myScanner.hasNext(); 
      
    while (input4 != true){
      System.out.println("Please enter the time the class meets");
      myScanner.nextLine();
      input4 = myScanner.hasNext();
      }
    
    String classTime = myScanner.next();
      
      System.out.println(classTime);
   
          
    
    
    
      //instructor name
    
    System.out.println("Please enter the instructor name");
    
    boolean input5 = myScanner.hasNext(); 
      
    while (input5 != true){
      System.out.println("Please enter the instructor name");
      myScanner.nextLine();
      input5 = myScanner.hasNext();
      }
    
    String instructorName = myScanner.next();
      
      System.out.println(instructorName);
    
    
    
    
      //number of students
    
    System.out.println("Please enter the number of students");
    
    boolean input6 = myScanner.hasNextInt(); 
      
    while (input6 != true){
      System.out.println("Please enter the number of students");
      myScanner.nextLine();
      input6 = myScanner.hasNextInt();
      }
    
    int numberOfStudents = myScanner.nextInt();
      
      System.out.println(numberOfStudents);
    
    
  }
  
}