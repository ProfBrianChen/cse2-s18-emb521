/* Nell Beatty
- CSE2
- 4/24
- hw10
- this code contructs a city, 
- assigns values for each populations of each block,
- print out these values as a grid
- changes the values of the population based on where robots are randomly placed,
- prints out these vales as a grid
- moves all robots over one block and changes the value wherever it is placed
- prints out these values as a grid
- repeats 5 times
*/
 
import java.util.Random;
 
public class RobotCity
{
 
  public static void main(String [] args)
  {
    Random myRandom = new Random();
    
    
    //declaring city array
    int[][] cityArray;
    
    int i = 0;
    
    for (i = 0; i <=4; i++)
    {
      System.out.printf("\n\n\n THIS IS LOOP %d: \n\n\n", i + 1);
       
    //calling buildCity
    cityArray= buildCity();
    
    //print it
    display(cityArray);
    
    //calling invade
    int numberOfRobots = 0; //this is the number of robots
    numberOfRobots = myRandom.nextInt(cityArray.length * cityArray[0].length - 1) + 1; //making sure its not more than the total number of blocks
    invade(cityArray, numberOfRobots);
      
    //printing after invade
    System.out.println("Populations after invasions of robots. Robots: " + numberOfRobots);
    display(cityArray);
    
    //calling update
    update(cityArray);
      
    //priting after update
    System.out.println("Populations after robots move east: ");
    display(cityArray);
    
    }//end of for loop
      
    
  
  }//end of main()

  
  //buildCity():
    //randomly generates the east-west and north-south dimensions from 10-15
    //assigns a random integers between 100 and 999 to represent the population 
  public static int[][] buildCity()
  {
    Random myRandom = new Random();
    
    //randomly generating dimensions (from 10 to 15)
    int randomColumns = myRandom.nextInt(5)+10; 
    int randomRows = myRandom.nextInt(5)+10; 
    
    //giving cityArray these dimensions
    int[][] cityArray = new int[randomColumns][randomRows];
    int randomNum = 0;
    
    //assigning a population number to each block (from 100 to 999)
    int i = 0; //represents the columns
    int j = 0; //represents the rows
    
    for (i = 0; i < cityArray.length; i++) //goes from east to west
    {
      for (j = 0; j < cityArray[i].length; j++) //south to north
      {
         cityArray[i][j] = myRandom.nextInt(899)+100;
      }
    }//end of for loop
  
    
    return(cityArray);
    
  }//end of buildCity()
  

  
  
  
    //display():
  //accepts the city array as input
  //prints the city's block level populations out 
  public static void display( int[][] cityArray)
  {
    int i = 0; //columns
    int j = 0; //rows
    
    //using for loops line 55
    for (j = (cityArray[j].length -1); j >= 0; j--) //this loop is for each row, starting at the top (north)
    {
      for (i = 0; i < cityArray.length; i++) //print each row from east to west (columns)
      {
         System.out.printf("%5d ", cityArray[i][j]);
      }
       System.out.println("");
    }//end of for loops
     
    
  }//end of display()
    
    //invade():
  //accepts the city array and a random interger k as input. 
  //k is the number of invading robots
  //for each robot, 
  //randomly generate block coordinates in the city where the robot lands,
  //makes sure no robot lands on top of another robot
  //sets the value of the block the robot lands on the the negative value of its orginial value
 
public static void invade( int[][] cityArray, int k )
{
     Random myRandom = new Random();
     int i = 0;
     int robotRow = 0;
     int robotColumn = 0;
  
     for (i = 0; i < k ; i++) //creating a random row and column for each robot
     {
       do
       {
        robotColumn = myRandom.nextInt(cityArray.length);
        robotRow = myRandom.nextInt(cityArray[robotColumn].length); 
       } while (cityArray[robotColumn][robotRow] < 0); //making sure this coordinate doesn't already have a robot
         
       cityArray[robotColumn][robotRow]*= -1; //making the population number negative
     }
 
 }//end of invade
  
    //update():
  //accepts the city array as input
  //moves the robot to the next block east, changes that block to negative value
  //robots that go off the grid dissapear
  public static void update( int[][] cityArray)
  {
    int i = 0;
    int j = 0;
    
    //using for loops line 55 //for loops starting east, so they all don't become positive
    for (i = (cityArray.length - 1); i >= 0 ; i--) //this loop is for each column from east to west
    {
      for (j = 0; j < cityArray[i].length; j++) //this loops is for each row in the column
      {
          if (cityArray[i][j] < 0) //if there has been a robot here
          {
            cityArray[i][j] = -1*(cityArray[i][j]); //here, the value goes back to positive
            if (i < (cityArray.length -1)) //the last column of the city, if your less than this last one, then you can move the robot over
            {
              cityArray[i+1][j] = -1*(cityArray[i+1][j]); //move robot to next column
            }
          }//end of inner loop
       }//end of outer loop
    
    }
        
  } 
  
  
}
    
  
  

  
