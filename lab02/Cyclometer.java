//////////////////////////////////////////
/// Nell Beatty, 2/2/18, CSE 2, Cyclometer
/////////////////////////////////////////
/// My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time. For two trips, given time and rotation count, your program should
//print the number of minutes for each trip
//print the number of counts for each trip
//print the distance of each trip in miles
//print the distance for the two trips combined
//MPG miles per gallons

public class Cyclometer {
  
  public static void main(String[] args) {

    
    double secsTrip1=480.0; //480 seconds in Trip 1
    double secsTrip2=3220.0; // seconds for Trip 2
    int countsTrip1=1561; // counts for Trip 1
    int countsTrip2=9037; // counts for Trip 2
    
    double wheelDiameter=27.0; //wheel diameter
      double PI=3.14159; //pi
   double feetPerMile=5280.0; //feet per miles
    double inchesPerFoot= 12.0; //inches per foot
     double secondsPerMinute=60.0; //seconds per minute
      double distanceTrip1, distanceTrip2, totalDistance; //
    
    
    ///printing out the variables
    
    System.out.println ("Trip 1 took  " +
                        (secsTrip1/secondsPerMinute)+"   minutes and had   "+ 
                        countsTrip1 +" counts.") ;
    
    System.out.println  ("Trip 2 took  " +
                         (secsTrip2/secondsPerMinute)+"   minutes and had  "+ 
                         countsTrip2+" counts.") ;
    
    
    /// calculating the minutes, counts
      
      
      distanceTrip1=countsTrip1*wheelDiameter*PI; //128,767.979 is the value of distance trip 1 
    
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    
	distanceTrip1/=inchesPerFoot*feetPerMile; //2.0323229 is the value of distance trip 1
    
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //12.0982452 is the value of trip 2
    
	totalDistance=distanceTrip1+distanceTrip2; //14.1305681 is the total distance of trip 1 and 2

      // seconds it took trip 1
      
      //seconds it took trip 2
     
      
    ///printing out the distances
    
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
    
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
    
	System.out.println("The total distance was "+totalDistance+" miles");

  } 
}