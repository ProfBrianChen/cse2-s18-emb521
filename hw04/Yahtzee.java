//////////////
// Nell Beatty
// hw04
// Yahtzee.java
// 2/20
//this program plays one game of Yahtzee 


import java.util.Scanner; 

public class Yahtzee {
 
  public static void main(String[] args) {
 
//creating the scanner 
    Scanner myScanner = new Scanner ( System.in ) ; //asking the user if they want to get randomly generated numbers or write their own 5 digit number
    System.out.println("Type 1 for randomly generated numbers and 2 for writing your own 5 digit number");
     double answer = myScanner.nextDouble();
    int diceOne= (int) (Math.random() * 5 + 1);  //defining the dice 
    System.out.println();
    int diceTwo= (int) (Math.random() * 5 + 1);
    System.out.println();
    int diceThree= (int) (Math.random() * 5 + 1);
    System.out.println();
    int diceFour= (int) (Math.random() * 5 + 1);
    System.out.println();
    int diceFive= (int) (Math.random() * 5 + 1);
    if (answer == 1){ //if they type in 1, yes, they get randomly generated numbers
      System.out.println("dice one = " + diceOne);
      System.out.println("dice two = " + diceTwo);
      System.out.println("dice three = " + diceThree);
      System.out.println("dice four = " + diceFour);
      System.out.println("dice five = " + diceFive);  
    }
    
    
    if (answer == 2){ //if they type 2, no, they will be asked to type in a five digit number
      System.out.println("Type in your five digit number with only digits 1 through 6");
    int fiveDigitNumber = myScanner.nextInt();
    System.out.println(fiveDigitNumber);
    if (fiveDigitNumber>=11111 && fiveDigitNumber<=66666){
      diceOne = fiveDigitNumber/10000;
      System.out.println("Dice one= " + diceOne);
      fiveDigitNumber = fiveDigitNumber - (diceOne * 10000);
      diceTwo = fiveDigitNumber/1000; 
      System.out.println("Dice two= " + diceTwo);
      fiveDigitNumber = fiveDigitNumber - (diceTwo * 1000);
      diceThree = fiveDigitNumber/100;
      System.out.println("Dice three= " + diceThree);
      fiveDigitNumber = fiveDigitNumber - (diceThree * 100);
      diceFour = fiveDigitNumber/10;
      System.out.println("Dice four= " + diceFour);
      fiveDigitNumber = fiveDigitNumber - (diceFour * 10);
      diceFive = fiveDigitNumber/1;
       System.out.println("Dice five= " + diceFive);
    } 
    else { 
      System.out.println("Type in a five digit number");
      System.exit (0);
    }}
    
  int aces = 0; //upper categories
    int twos = 0;
    int threes = 0;
    int fours = 0; 
    int fives = 0;
    int sixes = 0;
    int threeOfAKind = 0; //lower categories
    int fourOfAKind = 0;
    int fullHouse = 0;
    int smStraight = 0;
    int lgStraight = 0;
    int yahtzee = 0;
    int chance = 0; 
    int totalAces = 0; 
    int totalTwos = 0;
    int totalThrees = 0;
    int totalFours = 0;
    int totalFives = 0;
    int totalSixes = 0;
    
 //adding values to the varibles for how many dice of the same value are in the roll
    //totalAces
    if (diceOne == 1){
      totalAces = totalAces + 1;
    }
    if (diceTwo == 1){
      totalAces = totalAces + 1;
    }
    if (diceThree == 1){
      totalAces = totalAces + 1;
    }
    if (diceFour == 1){
      totalAces = totalAces + 1;
    }
    if (diceFive == 1){
      totalAces = totalAces + 1;
    }
 
    //totalTwos
    if (diceOne == 2){
      totalTwos = totalTwos + 1;
    }
    if (diceTwo == 2){
      totalTwos = totalTwos + 1;
    }
    if (diceThree == 2){
      totalTwos = totalTwos + 1;
    }
    if (diceFour == 2){
      totalTwos = totalTwos + 1;
    }
    if (diceFive == 2){
      totalTwos = totalTwos + 1;
    }
 
    //totalThrees
    if (diceOne == 3){
      totalThrees = totalThrees + 1;
    }
    if (diceTwo == 3){
      totalThrees = totalThrees + 1;
    }
    if (diceThree == 3){
      totalThrees = totalThrees + 1;
    }
    if (diceFour == 3){
      totalThrees = totalThrees + 1;
    }
    if (diceFive == 3){
      totalThrees = totalThrees + 1;
    }
 
    //totalFours
    if (diceOne == 4){
      totalFours = totalFours + 1;
    }
    if (diceTwo == 4){
      totalFours = totalFours + 1;
    }
    if (diceThree == 4){
      totalFours = totalTwos + 1;
    }
    if (diceFour == 4){
      totalFours = totalFours + 1;
    }
    if (diceFive == 4){
      totalFours = totalFours + 1;
    }
   
    //totalFives
    if (diceOne == 5){
      totalFives = totalFives + 1;
    }
    if (diceTwo == 5){
      totalFives = totalFives + 1;
    }
    if (diceThree == 5){
      totalFives = totalFives + 1;
    }
    if (diceFour == 5){
      totalFives = totalFives + 1;
    }
    if (diceFive == 5){
      totalFives = totalFives + 1;
    }
    
    //totalSixes
    if (diceOne == 6){
      totalSixes = totalSixes + 1;
    }
    if (diceTwo == 6){
      totalSixes = totalSixes + 1;
    }
    if (diceThree == 6){
      totalSixes = totalSixes + 1;
    }
    if (diceFour == 6){
      totalSixes = totalSixes + 1;
    }
    if (diceFive == 6){
      totalFives = totalSixes + 1;
    }
    
//if statements for upper sections
//adding values to the variables for each component on the upper half of the score card

    //diceOne upper section
    if (diceOne == 1){
      aces = aces + 1;
    }
    if (diceOne == 2){
      twos = twos + 2;
    } 
    if (diceOne == 3){
      threes = threes + 3;
    }
    if (diceOne == 4){
      fours = fours + 4;
    }
    if (diceOne == 5){
      fives = fives + 5;
    }
    if (diceOne == 6){
      sixes = sixes + 6;
    }
 
    //diceTwo upper section
    if (diceTwo == 1){
      aces = aces + 1;
    }
    if (diceTwo == 2){
      twos = twos + 2;
    } 
    if (diceTwo == 3){
      threes = threes + 3;
    }
    if (diceTwo == 4){
      fours = fours + 4;
    }
    if (diceTwo == 5){
      fives = fives + 5;
    }
    if (diceTwo == 6){
      sixes = sixes + 6;
    }
    
    //diceThree upper section
    if (diceThree == 1){
      aces = aces + 1;
    }
    if (diceThree == 2){
      twos = twos + 2;
    } 
    if (diceThree == 3){
      threes = threes + 3;
    }
    if (diceThree == 4){
      fours = fours + 4;
    }
    if (diceThree == 5){
      fives = fives + 5;
    }
    if (diceThree == 6){
      sixes = sixes + 6;
    }
    
     //diceThree upper section
    if (diceFour == 1){
      aces = aces + 1;
    }
    if (diceFour == 2){
      twos = twos + 2;
    } 
    if (diceFour == 3){
      threes = threes + 3;
    }
    if (diceFour == 4){
      fours = fours + 4;
    }
    if (diceFour == 5){
      fives = fives + 5;
    }
    if (diceFour == 6){
      sixes = sixes + 6;
    }
    
    //diceFive upper section
    if (diceFive == 1){
      aces = aces + 1;
    }
    if (diceFive == 2){
      twos = twos + 2;
    } 
    if (diceFive == 3){
      threes = threes + 3;
    }
    if (diceFive == 4){
      fours = fours + 4;
    }
    if (diceFive == 5){
      fives = fives + 5;
    }
    if (diceFive == 6){
      sixes = sixes + 6;
    }
   
//if statements for lower section
//adding values to the variables for each component on the lower half of the score card
    
//threeOfAKind 
    if (totalAces == 3){
      threeOfAKind = threeOfAKind + (3*1);
    }
    if (totalTwos == 3){
      threeOfAKind = threeOfAKind + (3*2);
    }
    if (totalThrees == 3){
      threeOfAKind = threeOfAKind + (3*3);
    }
    if (totalFours == 3){
      threeOfAKind = threeOfAKind + (3*4);
    }
    if (totalFives == 3){
      threeOfAKind = threeOfAKind + (3*5);
    }
    if(totalSixes == 3) {
      threeOfAKind = threeOfAKind + (3*6);
    }
  
//fourOfAKind
    if (totalAces == 4){
      fourOfAKind = fourOfAKind + (4*1);
    }
    if (totalTwos == 4){
      fourOfAKind = fourOfAKind + (4*2);
    }
    if (totalThrees == 4){
      fourOfAKind = fourOfAKind + (4*3);
    }
    if (totalFours == 4){
      fourOfAKind = fourOfAKind + (4*4);
    }
    if (totalFives == 4){
      fourOfAKind = fourOfAKind + (4*5);
    }
    if (totalSixes == 4){
      fourOfAKind = fourOfAKind + (4*6);
    }
    
//fullHouse
  
   //when totalAces=2
   if (totalAces==2 && totalTwos==3){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==2 && totalThrees==3){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==2 && totalFours==3){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==2 && totalFives==3){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==2 && totalSixes==3){
     fullHouse = fullHouse + 25;
   }
  
   //when totalAces=3
   if (totalAces==3 && totalTwos==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalThrees==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalFours==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalFives==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalSixes==2){
     fullHouse = fullHouse + 25;
  }
    
   //when totalTwos=2
   if (totalTwos==2 && totalAces==3){
     fullHouse = fullHouse + 25;
   }
  if (totalTwos==2 && totalThrees==3){
     fullHouse = fullHouse + 25;
   }
  if (totalTwos==2 && totalFours==3){
     fullHouse = fullHouse + 25;
   }
  if (totalTwos==2 && totalFives==3){
     fullHouse = fullHouse + 25;
   }
  if (totalTwos==2 && totalSixes==3){
     fullHouse = fullHouse + 25;
   }
    
   //when totalTwos=3
  if (totalTwos==3 && totalAces==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalThrees==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalFours==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalFives==2){
     fullHouse = fullHouse + 25;
   }
  if (totalAces==3 && totalSixes==2){
     fullHouse = fullHouse + 25;
  }
    
    //when totalThrees=2
    if (totalThrees==2 && totalAces==3){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==2 && totalTwos==3){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==2 && totalFours==3){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==2 && totalFives==3){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==2 && totalSixes==3){
     fullHouse = fullHouse + 25;
   }
    
    //when totalThrees=3
    if (totalThrees==3 && totalAces==2){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==3 && totalTwos==2){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==3 && totalFours==2){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==3 && totalFives==2){
     fullHouse = fullHouse + 25;
   }
  if (totalThrees==3 && totalSixes==2){
     fullHouse = fullHouse + 25;
   }
    
    //when totalFours=2
     if (totalFours==2 && totalAces==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==2 && totalTwos==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==2 && totalThrees==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==2 && totalFives==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==2 && totalSixes==3){
     fullHouse = fullHouse + 25;
   }
    
    //when totalFours=3
    if (totalFours==3 && totalAces==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==3 && totalTwos==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==3 && totalThrees==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==3 && totalFives==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFours==3 && totalSixes==2){
     fullHouse = fullHouse + 25;
   }
  
    //when totalFives=2
    if (totalFives==2 && totalAces==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==2 && totalTwos==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==2 && totalThrees==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==2 && totalFours==3){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==2 && totalSixes==3){
     fullHouse = fullHouse + 25;
   }
    
    //when totalFives=3
    if (totalFives==3 && totalAces==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==3 && totalTwos==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==3 && totalThrees==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==3 && totalFours==2){
     fullHouse = fullHouse + 25;
   }
  if (totalFives==3 && totalSixes==2){
     fullHouse = fullHouse + 25;
   }
    
    //when totalSixes=2
    if (totalSixes==2 && totalAces==3){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==2 && totalTwos==3){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==2 && totalThrees==3){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==2 && totalFours==3){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==2 && totalFives==3){
     fullHouse = fullHouse + 25;
   }
    
    //when totalSixes=3
    if (totalSixes==3 && totalAces==2){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==3 && totalTwos==2){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==3 && totalThrees==2){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==3 && totalFours==2){
     fullHouse = fullHouse + 25;
   }
  if (totalSixes==3 && totalFives==2){
     fullHouse = fullHouse + 25;
   }
  
    //smStraight
  
  if (totalAces ==1 && totalTwos ==1 && totalThrees==1 && totalFours==1){
    smStraight = smStraight + 30; 
  }
  if (totalTwos ==1 && totalThrees==1 && totalFours==1 && totalFives==1){
    smStraight = smStraight + 30; 
  }
  if (totalThrees ==1 && totalFours ==1 && totalFives ==1 && totalSixes==1){
    smStraight = smStraight + 30; 
  }

    //lgStraight
    
   if (totalAces ==1 && totalTwos ==1 && totalThrees ==1 && totalFours==1 && totalFives ==1){
     lgStraight = lgStraight + 40;
   }
   if (totalTwos ==1 && totalThrees ==1 && totalFours==1 && totalFives ==1 && totalSixes ==1){
     lgStraight = lgStraight + 40;
   }

    //yahtzee
    if (totalAces == 5){
      yahtzee = yahtzee + 50;
    }
    if (totalTwos == 5){
      yahtzee = yahtzee + 50;
    } 
    if (totalThrees == 5){
      yahtzee = yahtzee + 50;
    }
    if (totalFours == 5){
      yahtzee = yahtzee + 50;
    }
    if (totalFives == 5){
      yahtzee = yahtzee + 50;  
    }
    if (totalSixes == 5){
      yahtzee = yahtzee + 50;
    }
    
    //Chance
 chance = diceOne+diceTwo+diceThree+diceFour+diceFive;
  

//printing out all the categories on the score card
 System.out.println("Aces= " + aces); 
 System.out.println("Twos= " + twos);
 System.out.println("Threes= " + threes);
 System.out.println("Fours= " + fours);
 System.out.println("Fives= " + fives);
 System.out.println("Sixes= " + sixes);
 System.out.println("3 of a kind= " + threeOfAKind);
 System.out.println("4 of a kind= " + fourOfAKind);
 System.out.println("Full house= " + fullHouse);
 System.out.println("Sm. Straight= " + smStraight);
 System.out.println("Lg. Straight= " + lgStraight);
 System.out.println("YAHTZEE= " + yahtzee);
 System.out.println("Chance= " + chance);
        

//totals 

  //defining the variable for total of the upper section
  int totalUpperSection = aces+twos+threes+fours+fives+sixes;
  
  //adding the possible bonus points 
  if (totalUpperSection >= 63){
    totalUpperSection = totalUpperSection + 35;
  }
  
  //printing the total upper section points
  System.out.println("Total Upper Section = " + totalUpperSection);
 
  //defining the variable for total of the lower section
  int totalLowerSection = +threeOfAKind+fourOfAKind+fullHouse+smStraight+lgStraight+yahtzee+chance; 

  //priting the total lower section points
  System.out.println("Total Lower Section = " + totalLowerSection);
  
  //defining the variable for the the grand total (upper and lower section points)
  int grandTotal = totalUpperSection + totalLowerSection;
  //printing the grand totalUpperSection
  System.out.println("Grand Total = " + grandTotal);
  }

} 
