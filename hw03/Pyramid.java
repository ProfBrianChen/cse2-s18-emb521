//////////////////////////////////////////
/// CSE 2 hw03 Pyramid
/// Nell Beatty
/// 2/13/18

//calculating the volume of a pyramid --> length * width * height all over 3. 


import java.util.Scanner;

public class Pyramid {
  
  
  
  public static void main(String[] args) {
    
     Scanner myScanner = new Scanner ( System.in ) ;
    
    
    //asks users for the height of the pyramid
    
    System.out.println ("enter the height of the pyramid in the form xx.xx");
   
    double heightOfPyramid = myScanner.nextDouble();
    
    //the height of the pyramid is 15
      
    
    
    
    
    //asks users for the length of the pyramid
    
    System.out.println ("enter the length of the pyramid in the form xx.xx");
    
    double lengthOfPyramid = myScanner.nextDouble();
    
    //the length of the pyramid is 25
    
    
    
    
    //3 is the number in the formula for the volume of a pyramid
    double numberDivided = 3;
    //the formula for volume of a pyramid is l*w*h/3
    //in this case, the length and width are the same because there is a square base
    
    
    
  
      double volumeOfPyramid = (heightOfPyramid * lengthOfPyramid * lengthOfPyramid) / numberDivided;   //volume of pyramid
    System.out.println("volume of the pyramid" + volumeOfPyramid);
    
    //the volume of the pyramid 3125
      
  }
  
}

