//////////////////////////////////////////
/// CSE 2 hw03 Convert
/// Nell Beatty
/// 2/13/18

import java.util.Scanner; 

public class Convert {
  
  
  
  public static void main(String[] args) {
   
   
    Scanner myScanner = new Scanner ( System.in ) ;
    
    System.out.println ("enter the number of acres of land affected by the hurricane precipitation in the form xx.xx");
      //asks users for a double that represents the number of acres of land affected by the hurricane precipitation
      //the affected area is 23523.25 acres
    
    double acresOfLand = myScanner.nextDouble(); 
    
    
    
      
    System.out.println ("enter the number for the average rain dropped in the form xx.xx");
      //asks users for a double that represents the average number for inches of rain dropped 
      // the average amount of rainfall in the affected area is 45 inches
   
    double inchesOfRain = myScanner.nextDouble(); 
    
    
    double gallons = acresOfLand * inchesOfRain;
    //converting acres and inches to gallons
    
    double cubicMiles =  gallons * (9.0816859724455 * Math.pow (10,-13));
    //converting gallons to cubic miles 
      
  System.out.println("cubicMiles" + Math.round(cubicMiles * 100000000d)/100000000d);

    
  
    
    
  }
  
}

