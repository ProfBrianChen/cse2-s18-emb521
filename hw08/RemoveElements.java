/* Nell Beatty
- CSE2
- 4/10
- hw08
- RemoveElements
-this program uses three methods to make an array with a random input, deletes or changes certain numbers in the array, and makes the array shorter 

*/
  
 import java.util.Scanner;
  
import java.util.Random;

public class RemoveElements
{
 
public static void main(String [] arg)
 {
    
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do
  {
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
    
  }//end of main()
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++)
  {
  	if(j>0)
    {
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
return out;
  } //end of listArray()
  
  
  //BELOW: the methods that I have to do 
  
	
	//randomInput()
	//generates and array with 10 rando ints with 0 to 9
	//returns filled array
  public static int[] randomInput()
  {
    int size = 10;
    int[] theReturnArray= new int[size];
    int i = 0;
    Random myRandom = new Random();
    int randomNum = 0;
      
      for (i = 0; i< size; i++)
      {
        randomNum = myRandom.nextInt(size); //making the randomNum
        theReturnArray[i] = randomNum; //putting the randomNum at the certain index
      }
 
    return(theReturnArray);
    
  } //end of randomInput()
  
  
	//delete()
	//this method creates a new array that has one member fewer than list
	//the new array is composed of all the same members except the member in in position pos
   public static int[] delete(int [] list,int pos)
  {
     int i = 0;
     int[] theReturnArray1= new int[list.length - 1];
     int index = 0;
     
     for (i= 0; i < list.length; i++)
     {
       
       if (i == pos)
       {
         continue;
       }
         
       else
       {
         theReturnArray1[index] = list[i];
         index ++;
       }
       
       
     }//end of for loop
     
    
     return (theReturnArray1);
     
  } //end of delete()
  
  
  
  
  
  //remove()
	//this method deletes all the elements that are equal to target
	//returns a new list without those new elements
   public static int[] remove(int[] list, int target)
  {
     int i = 0;
     
     int targetCount = 0; //how many instances of target there are in the array
     
     int index = 0;
     
     for (i = 0; i < list.length; i++)
     {
       if (list[i] == target)
       {
         targetCount++;
       }
       
     }
     
     int[] theReturnArray1= new int[list.length - targetCount];
     
     int j = 0;
     
     for (j = 0; j < list.length; j ++)
     {
     if (list[j] == target)
       {
         continue;
       }
       else
       {
        theReturnArray1[index++] = list[j];
       }
     }
     return (theReturnArray1);
       
  } //end of randomInput()
  
 }//end of class

 

  
 