/* Nell Beatty
- CSE2
- 4/10
- hw08
- CSE2 Linear
-this code asks the user to enter the final grades from CSE2, makes an array of grades, lets the user search for certain grades (using both binary and linear search) and scrambles the array randomly 


*/

import java.util.Scanner; 

import java.util.Random;

public class CSE2Linear
{
 
  public static void main(String[] args) //main method
{
  int aGrade = 0;
  int prevGrade = -1;
  Scanner myScanner = new Scanner(System.in);
    
  int[] studentGrades = new int[15]; //declaring the array
  int i = 0; 
    
 System.out.println("Please enter 15 ascending ints for final grades in CSE2:");
    
  for(i = 0; i < 15; i++) //creating the array of grades
  {
    do 
    {
      
      aGrade = myScanner.nextInt(); //making a variable for the user's input
      studentGrades[i]= aGrade; //making studentGrade[i] this number
    } while (studentGrades[i] < prevGrade || aGrade < 0 || aGrade > 100 ); //making sure the grades are in ascending order and are greater or equal 0 and less than or equal to 100
    studentGrades[i] = aGrade;
    prevGrade= studentGrades[i];    
    
  }//end of for loop
    
// use binary search to find the entered grade
  System.out.println("Enter a grade to search for");
  int binarySearchGrade = 0;
  binarySearchGrade = myScanner.nextInt();
    binarySearch(binarySearchGrade, studentGrades);
    
      
// scramble the array and print it out
    randomScrambling(studentGrades);
    System.out.println("Here is the random list: ");
    for (i = 0; i < studentGrades.length; i++)
    {
      System.out.print(studentGrades[i] + " , ");
    }
    System.out.println();
      
    
// use linear search to find the entered grade
  System.out.println("Enter a grade to search for");
  int linearSearchGrade = 0; //the number the user wants to search for
  linearSearchGrade = myScanner.nextInt();
  linearSearch(linearSearchGrade, studentGrades, true);
    
  } //end of main
  
  
  
  //LINEAR SEARCH METHOD
  
  public static boolean linearSearch(int searchNumber, int[] list, boolean printMode) //method for linear search
  {
    int j=0;
    boolean found = false;
    
    for (j = 0; j < list.length; j++) 
    {
      if (list[j] == searchNumber) //element at index j has to match the search number for it to exist
      {
        break;
      }
      
    }//end of for loop
    
    
      if (j==list.length) //printing the answers
      {
        if (printMode)
          System.out.println("Linear search: the number " + searchNumber + " was not found with " + j + " iterations ");
      }
      else 
      {
        if (printMode)
          System.out.println("Linear search: the number " + searchNumber + " was found with " + (j+1) + " iterations ");
       found = true;
      }
    
    
   
    return (found); //end of linearSearch
  } 
  
  //END OF LINEAR SERACH METHOD
  
    
    
  //BINARY SEARCH METHOD
  
  public static boolean binarySearch(int searchNumber, int[] list) //method for binary search
  {
    int k = 0;
    int first = 1;
    int last = list.length;
    boolean found = false;
    int iterations = 0;
    int middle = 1;
    
  
    while (last > (first + 1)) //search until you can't take another midpoint, no more to search
    {
       middle = (first + last)/2;
       iterations ++; 
      if (list[middle] == searchNumber)
      {
        found = true;
        break;
      }
      else //adjust first and last number
      {
        if (list[middle] > searchNumber)
        {
          last = middle; //making the list your looking in shorter
        }
        else
        {
          first = middle; //making the list your looking in shorter
        }
      }
    }
    
    if (found) //printing the answer
    {
      System.out.println("Binary search: the number " + searchNumber + " was found with " + iterations + " iterations ");
    }
      else 
    {
      System.out.println("Binary search: the number " + searchNumber + " was not found with " + iterations + " iterations ");
    }
      
   
   return (found); 
    
  }
  
 //END OF BINARY SEARCH METHOD 
  
  
  
  //RANDOM SCRAMBLING METHOD
  
  public static void randomScrambling(int[] list) //method for random scrambling
  {
    
    Random myRandom = new Random(); 
    int i = 0;
    int randomNum = 0;
    int[] newList = new int[list.length];
    int j = 0;
    int[] randomList = new int[list.length]; //this list keeps random indexes used
    boolean unique; 
   
      for (i=0; i<list.length; i++) //making a copy of the list
      {
        newList[i] = list[i];
        randomList[i] = -1; //making sure theres RandomList doesn't already have valid random numbers
      }
   
      for (j=0; j < list.length; j++) //changing the list that was passed in
      {
        unique = false;
        while (!unique) 
        {
          randomNum = myRandom.nextInt(list.length);
          unique = !linearSearch(randomNum, randomList, false); //calling linearSearch to make sure there no repeats for indexes
        }
        randomList[j] = randomNum; //putting list together of all the random indexes
        list[j] = newList[randomNum];
      } 
  
    
  } //end of randomScrambling
  
  
  //END OF RANDOM SCRAMBLING METHOD
 
  
} //end of class

