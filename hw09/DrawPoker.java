/*
Nell Beatty
4/14/17
DrawPoker.java
this code plays the game of poker
*/

import java.util.Scanner; 

import java.util.Random;

public class DrawPoker
{
  
  
 public static void main(String[] args) //main method
{
   //making an array for the deck of cards
  int[] deckOfCards = new int[52];
   
   System.out.print("Here's the sorted the deck of cards");
   //giving value to each element
  int i = 0;
   for ( i = 0 ; i < 52 ; i ++ )
   {
     deckOfCards[i] = i;
   }
  
   
   //getting the shuffled deck
    shuffleMethod(deckOfCards);
     
   
   
   //giving cards to two players
   int[] hand1 = new int[5];
   int[] hand2 = new int[5];
   int j = 0;
   //for loops to give the first card to player 1, second card to player 2, third to player one, and so on
      for ( j = 0 ; j < 5 ; j ++ )
      {
          hand1[j] = deckOfCards[j * 2];
          hand2[j] = deckOfCards[j * 2 + 1];
      }//end of loop
   
   //printing hand1 and hand2
   System.out.println("Here is the hand for player 1");
   printHand(hand1);
   System.out.println("Here is the hand for player 2");
   printHand(hand2);

   //determing who wins
   
   //assign value to the hand
   boolean match = false;
   int hand1Value = 0;
   int hand2Value = 0;
   //values for each of the hand types
   int pairValue = 1;
   int threeOfAKindValue = 2;
   int flushValue = 3;
   int fullHouseValue = 0;
   
   //giving value to each player, based on what the methods return (what their hand holds)
   match = pair(hand1);
   if (match == true)
   {
      hand1Value = pairValue;
   }
     
   match = pair(hand2);
   if (match == true)
   {
      hand2Value = pairValue;
   }
   
   match = threeOfAKind(hand1);
   if (match == true)
   {
      hand1Value = threeOfAKindValue;
   }
   
   match = threeOfAKind(hand2);
   if (match == true)
   {
      hand1Value = threeOfAKindValue;
   }
   
   match = flush(hand1);
   if (match == true)
   {
      hand1Value = flushValue;
   }
   
   match = flush(hand2);
   if (match == true)
   {
      hand2Value = flushValue;
   }
   
   match = fullHouse(hand1);
   if (match == true)
   {
      hand1Value = fullHouseValue;
   }
   
    match = fullHouse(hand2);
   if (match == true)
   {
      hand2Value = fullHouseValue;
   }
   
   //if both players have 0 (because they have nothing else in their hand), use highest number
   if (hand1Value == 0 && hand2Value == 0) 
   {
     hand1Value = highCard(hand1);
     hand2Value = highCard(hand2);
   }
     
   //print the winner
   if (hand1Value == hand2Value)
     System.out.println("This is a tie");
   else if (hand1Value < hand2Value)
     System.out.println("Player 2 wins");
   else 
     System.out.println("Player 1 wins");
     
 }//end of main() 
  
  //printHand() gives the suit and the value to each card in the hand
  public static void printHand(int[] hand)
  {
    int i = 0;
    int value = 0;
    int suit = 0;
    String theCardNum = "";
    String theSuit = "";
    
    
    for (i = 0; i < hand.length; i ++)
    {
      value = cardNum(hand[i]);
      suit = cardSuit(hand[i]);
   
      switch (value)
      {
        case 0:
          theCardNum = "Ace";
          break;
        case 1:
          theCardNum = "2";
          break;
       case 2:
          theCardNum = "3";
          break;
       case 3:
          theCardNum = "4";
           break;
       case 4:
          theCardNum = "5";
          break;
       case 5:
          theCardNum = "6";
          break;
       case 6:
          theCardNum = "7";
          break;
       case 7:
          theCardNum = "8";
          break;
       case 8:
          theCardNum = "9";
          break;
       case 9:
          theCardNum = "10";
          break;
       case 10:
          theCardNum = "Jack";
          break;
       case 11:
          theCardNum = "Queen";
          break;
      case 12:
          theCardNum = "King";
          break;
      }//end of switch statment
      
      switch (suit)
      {
        case 0:
          theSuit = "diamonds";
            break;
        case 1:
          theSuit = "clubs";
            break;
        case 2:
          theSuit = "hearts";
            break;
        case 3:
          theSuit = "spades";
            break;
      }//end of switch statement

      System.out.print(theCardNum + " of " + theSuit);
      System.out.println();
      
      }//end of for loop
    
    System.out.println();
      
    }//end of printHand()
 
  
   //method to shuffle the deckOfCards
   public static void shuffleMethod(int[] x)
   {
    Random myRandom = new Random(); 
    int i = 0;
    int randomNum = 0;
    int[] newList = new int[x.length]; 
    int j = 0;
    int[] randomList = new int[x.length]; //this list keeps random indexes used
    boolean unique; 
   
      for (i=0; i< x.length; i++) //making a copy of the list
      {
        newList[i] = x[i];
        randomList[i] = -1; //making sure theres RandomList doesn't already have valid random numbers
      }
   
      for (j=0; j < x.length; j++) //changing the list that was passed in
      {
        unique = false;
        while (!unique) 
        {
          randomNum = myRandom.nextInt(x.length);
          unique = !linearSearch(randomNum, randomList, false); //calling linearSearch to make sure there no repeats for indexes
        }
        randomList[j] = randomNum; //putting list together of all the random indexes
        x[j] = newList[randomNum];
      }
   
   }//end of shuffle method

  
  
  
  
  
  //method to make sure I shuffle cards correctly (I call this method in my shuffle method so there are no repeats)
  public static boolean linearSearch(int searchNumber, int[] list, boolean printMode) //method for linear search
  {
    int j = 0;
    boolean found = false;
    
    for (j = 0; j < list.length; j++) 
    {
      if (list[j] == searchNumber) //element at index j has to match the search number for it to exist
      {
        found = true;
        break;
      }
      
    }//end of for loop
   
    return (found); //end of linearSearch
  }//end of linearSearch()
  
   //pair() looks for two of the same elements in an one array
     public static boolean pair(int[] hand)
     {
       int i = 0;
       int m = 0;
       int k = 0;
       int count = 0;
       int cardValue = 0;
       int numOfSameValues = 0;
       int[] cardValueCount = new int[13]; //an array for the values of the cards
       boolean hasPair = false;
       
       //initlize counter array
       for (i = 0; i < cardValueCount.length; i++)
       {
         cardValueCount[i] = 0;
       }
       
       //count the number of repeats in each hand   
       for (m = 0; m < hand.length; m++)
       {
         cardValue = cardNum(hand[m]); //will get you a value 0 - 12
         cardValueCount[cardValue]++; //keep track of how many of each value you have
       }
       
       //see if there are any pairs
       
       for (k = 0; k < cardValueCount.length; k++)
       {
         if (cardValueCount[k] >= 2) //if theres a pair, then break and return true
         {
           hasPair = true;
           break;
         }
       }
       
       return (hasPair);
       
     }//end of pair()

  //this method returns the value of the card (from 0-12), regardless of suit
  public static int cardNum(int b)
  {
    int theCardValue = 0;
   
    theCardValue = b % 13;
    
    return (theCardValue); 
    
  }//end of cardNum()
  
  //this method returns the value of suit (0,1,2, or 3), regardless of the value
 public static int cardSuit(int b)
 {
   int theSuitValue = 0;
   
   theSuitValue = b/13; //(13) because 13 cards in each suit
  
   return (theSuitValue);
  
 }//end of cardSuit()
  
  
 public static boolean threeOfAKind(int[] hand)
 {
     int i = 0;
     int m = 0;
     int k = 0;
     int count = 0;
     int cardValue = 0;
     int numOfSameValues = 0;
     int[] cardValueCount = new int[13]; //an array for the values of the cards
     boolean hasThree = false;

     //initlize counter array
     for (i = 0; i < cardValueCount.length; i++)
     {
       cardValueCount[i] = 0;
     }

     //count the number of repeats in each hand   
     for (m = 0; m < hand.length; m++)
     {
       cardValue = cardNum(hand[m]); //will get you a value 0 - 12
       cardValueCount[cardValue]++; //keep track of how many of each value you have
     }

     //see if there are any pairs

     for (k = 0; k < cardValueCount.length; k++)
     {
       if (cardValueCount[k] >= 3) //if theres a 3, then break and return true
       {
         hasThree = true;
         break;
       }
     }

     return (hasThree);
   
 }//end of threeOfAKind
  
  
  //flush() looks if the hand has all the same suit
  public static boolean flush(int[] hand)
 {
     int i = 0;
     int m = 0;
     int k = 0;
     int count = 0;
     int cardValue = 0;
     int numOfSameValues = 0;
     int[] cardSuitCount = new int[4]; //an array for the values of the cards
     boolean flush = false;

     //initlize counter array
     for (i = 0; i < cardSuitCount.length; i++)
     {
       cardSuitCount[i] = 0;
     }

     //count the number of repeats in each hand   
     for (m = 0; m < hand.length; m++)
     {
       cardValue = cardSuit(hand[m]); //will get you a value 0 - 12
       cardSuitCount[cardValue]++; //keep track of how many of each value you have
     }

     //see if there are any pairs

     for (k = 0; k < cardSuitCount.length; k++)
     {
       if (cardSuitCount[k] >= 5) //if theres a pair, then break and return true
       {
         flush = true;
         break;
       }
     }

     return (flush);
   
 }//end of threeOfAKind
  
  //fullHouse() looks to see if there are two of a kind and three of a kind
  public static boolean fullHouse(int[] hand)
 {
     int i = 0;
     int m = 0;
     int k = 0;
     int count = 0;
     int cardValue = 0;
     int numOfSameValues = 0;
     int[] cardValueCount = new int[13]; //an array for the values of the cards
     boolean hasThree = false;
     boolean hasPair = false;
     boolean theAnswer = false;

     //initlize counter array
     for (i = 0; i < cardValueCount.length; i++)
     {
       cardValueCount[i] = 0;
     }

     //count the number of repeats in each hand   
     for (m = 0; m < hand.length; m++)
     {
       cardValue = cardNum(hand[m]); //will get you a value 0 - 12
       cardValueCount[cardValue]++; //keep track of how many of each value you have
     }

     //see if there are any pairs

     for (k = 0; k < cardValueCount.length; k++)
     {
       if (cardValueCount[k] >= 3) //if theres a 3
       {
         hasThree = true;
       }
       else if (cardValueCount[k] >=2) //if theres a pair
       {
         hasPair = true;
       }
     }
      
     if (hasPair == true && hasThree == true) //has to have both a pair and three of a kind
        theAnswer = true;
     
     return (theAnswer);
   
 }//end of threeOfAKind
  
  //highCard looks what the highest card is
  public static int highCard(int[] hand)
 {
    int i = 0;
    int highestCard = 0;
    int theCardValue = 0;
    
    for (i = 0; i < hand.length; i++)
    {
      theCardValue = cardNum(hand[i]);
      if (theCardValue > highestCard) //if the card at index i is great than the highest card, replace it with the current card
        
        highestCard = theCardValue;
    } 
    return (highestCard);
    
  }//end of highCard()
}//end of class
  
  
  