//////////////////////////////////////////
/// CSE 2 Hello World
/// Nell Beatty
/// 2/6/18
public class WelcomeClass {
  
  
  
  public static void main(String[] args) {
    // Prints "Hello, World" to the terminal window.
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("^ ^ ^ ^ ^ ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--J--K--0--0--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
  }
  
}

