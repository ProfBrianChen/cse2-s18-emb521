////////////////////////
//// CSE2
/// Arithmetic Class
/// Nell Beatty
/// 2/6/18

public class Arithmetic {
  
  
  
  public static void main(String[] args) {

///////declaring variables 

int numPants = 3; //number of pairs of pants

double pantsPrice = 34.98; //cost per pairs of paints


int numShirts = 2; //number of shirts

double shirtPrice = 24.99; //cost per shirt


int numBelts = 1; //number of belts

double beltCost = 33.99; //cost per belt


double paSalesTax = 0.06; //the pa sales tax rate

    
    
/////calculating total costs of items 
    
double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
    System.out.println("total cost of pants" + totalCostOfPants);
    

double totalCostOfShirts = numShirts * shirtPrice;   //total cost of shirts
    System.out.println("total cost of shirts" + totalCostOfShirts);

      
double totalCostOfBelts = numBelts * beltCost;   //total cost of belts
    System.out.println("total cost of belts" + totalCostOfBelts);
      

/////calculating sales tax for items
      
double totalSalesTaxOnPants = numPants * pantsPrice * paSalesTax; //total cost of sales tax on pants bought
    System.out.println("total sales tax on pants" + totalSalesTaxOnPants);
      
double totalSalesTaxOnShirts = numShirts * shirtPrice * paSalesTax; //total cost of sales tax on shirts bought
    System.out.println("total sales tax on shirts" + totalSalesTaxOnShirts);
  
double totalSalesTaxOnBelts = numBelts * beltCost * paSalesTax; //total cost of sales tax on belts bought
    System.out.println("total sales tax on belts" + totalSalesTaxOnBelts);

    
/////calculating Cost of Purchases
    
    
double totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //total cost of items purchased before sales tax
    System.out.println("total cost of purchases" + totalCostOfPurchases);
    
      
/////calculating total sales tax
    
double totalSalesTax = totalCostOfPurchases * paSalesTax ; //total sales tax on all purchase items
 System.out.println("total sales tax" + totalSalesTax);
    
 
/////calculating total paid for transaction
    
double totalPaidForTransaction = totalSalesTax + totalCostOfPurchases; //total paid for trasaction --> cost of all items plus total sales tax
  System.out.println("total paid for transaction" + totalPaidForTransaction);
    

  }
  
}



  
  
