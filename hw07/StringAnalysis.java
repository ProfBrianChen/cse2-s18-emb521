//Nell Beatty
//hw07
//StringAnalysis.java
//3/27

import java.util.Scanner; 

public class StringAnalysis {
 
  public static void main(String[] args) {
  
    Scanner a = new Scanner ( System.in );
    System.out.print("Type in a string you would like examined");
    String response = a.nextLine();
    System.out.print("Do you want to check a specific amount of characters? Enter yes or no");
    String yesOrNo = a.nextLine();
    while(yesOrNo.equals("yes") == false && yesOrNo.equals("no") == false){
      System.out.print("Please say yes or no: ");
      yesOrNo = a.nextLine();
    }
    if (yesOrNo.equals("yes")){
      System.out.print("How many characters do you want to check: ");
      while(a.hasNextDouble() == false) {
        System.out.print("Please enter a number: ");
        a.next();
      }
      double characterCount = a.nextDouble();
      System.out.println(analyse(response, characterCount));
    }
    else{
      System.out.println(analyse(response));
    }
  }
  
  public static boolean analyse (String phrase){
    int character = 0;
    while(character < phrase.length()){
      if(Character.isLetter(phrase.charAt(character))==false){
        return false;
      }
      else{
        character += 1;
        
      }
     }
     return true;
   
    }
  public static boolean analyse(String phrase, double count){
    int character = 0;
    while(character < phrase.length() && character < count){
      if(Character.isLetter(phrase.charAt(character)) == false){
        return false;
      }
      else{
        character += 1;
      }
      
    }
    return true;
  }
  }
