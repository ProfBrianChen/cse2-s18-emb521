//Nell Beatty
//hw07
//Area.java
//3/27

import java.util.Scanner; 

public class Area {
 
  public static void main(String[] args) {
  
    Scanner a = new Scanner ( System.in );
    
 System.out.print("Please choose a triangle, circle or rectangle by entering 'rectangle', 'triangle', or 'circle'");
 String answer = a.nextLine();
 while(answer.equals("rectangle") == false && answer.equals("triangle") == false && answer.equals("circle") && false){
   System.out.print("Your input was invalid. Please type in 'rectangle', 'triangle', or 'circle'");
   answer = a.nextLine();
 }
    
    
    
    if(answer.equals("rectangle"))
    {
      System.out.print("Please enter the length of the rectange");
      double length = itsADouble();
      System.out.print("Please enter the height of the rectangle");
      double height = itsADouble();
      System.out.println(rectangle(height, length));    
    }
 
    
    
    if(answer.equals("triangle"))
    {
      System.out.print("Please enter the base of the triangle");
      double length = itsADouble();
      System.out.print("Please enter the height of the triangle");
      double height = itsADouble();
      System.out.println(rectangle(height, length));
      
    }
    
    if(answer.equals("circle"))
    {
      System.out.print("Please enter the radius of the circle");
      double radius = itsADouble();
      System.out.println(circle(radius));
    }
    
  }
  
    public static double itsADouble(){
      Scanner b = new Scanner(System.in);
      while(b.hasNextDouble() == false){
      System.out.println("Please enter a double");
      b.next();
    }
      double number = b.nextDouble();
      return number;
    }
  
    public static double rectangle (double height, double length){
      double area = height * length;
      return area;
    }
     
    public static double triangle (double base, double height){
     double area = (height * base) / 2;
      return area;
     
    }
    
    public static double circle (double radius){
    double area = radius * radius * 3.14;
    return area;
      
    }
    
    
  }